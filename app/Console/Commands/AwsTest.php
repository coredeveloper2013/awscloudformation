<?php

namespace App\Console\Commands;

use App\Services\AwsCloudFormationService;
use Illuminate\Console\Command;

class AwsTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $aws = (new AwsCloudFormationService())->getStack()->toArray();
            foreach ($aws as $aa) {
                echo $aa;
            }
            dd($aws['StackSummaries']);

//            dd(gettype($aws->getStack()));
//            dd($aws->createStack());
        } catch (\Exception $exception) {
            throw $exception;
        }

    }
}
