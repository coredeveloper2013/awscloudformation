<?php


namespace App\Services;


use Aws\CloudFormation\CloudFormationClient;

class Configuration
{
    /**
     * @var CloudFormationClient
     */
    protected $client;

    protected $credentials;

    /**
     * AwsCloudFormationService constructor.
     */
    public function __construct()
    {
        $this->client = CloudFormationClient::factory([
            'credentials' => [
                'key' => config('services.ses.key'),
                'secret' => config('services.ses.secret')
            ],
            'version' => '2010-05-15',
            'region' => 'eu-west-1'
        ]);
    }
}
