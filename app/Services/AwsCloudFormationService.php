<?php


namespace App\Services;


use Aws\CloudFormation\CloudFormationClient;
use Aws\Credentials\Credentials;

class AwsCloudFormationService extends Configuration
{

    /**
     * @param $stack_name
     * @return \Aws\Result
     */
    public function createStack($stack_name)
    {
        $aws_json_path = base_path() . "/cloudformation.json";
        $json = file_get_contents($aws_json_path);

        $stack = $this->client->createStack([
            'StackName' => $stack_name,
            'OnFailure' => 'DELETE',
            'TemplateBody' => $json
        ]);

        return $stack;
    }

    /**
     * @return \Aws\Result
     */
    public function getStack()
    {
        $list = $this->client->listStacks();
        return $list;
    }
}
