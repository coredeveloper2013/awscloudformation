@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <div class="list-group">
                    <a href="{{ route('stack.index') }}" class="list-group-item list-group-item-action"
                       aria-current="true">
                        Wordpress Site List
                    </a>
                    <a href="{{ route('stack.create') }}" class="list-group-item list-group-item-action active">
                        Create Wordpress Site
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                <form class="card" method="post" action="{{ route('stack.store') }}">
                    @csrf
                    <div class="card-header">Create Wordpress Site</div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label class="form-label">Stack Name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Description</label>
                            <textarea class="form-control" name="description" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
