@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <div class="list-group">
                    <a href="{{ route('stack.index') }}" class="list-group-item list-group-item-action active"
                       aria-current="true">
                        Wordpress Site List
                    </a>
                    <a href="{{ route('stack.create') }}" class="list-group-item list-group-item-action">
                        Create Wordpress Site
                    </a>
                </div>
            </div>
            <div class="col-md-9">

                <div class="list-group">
                    @foreach($stack_list as $key=>$stack)
                        <a href="#"
                           class="list-group-item list-group-item-action {{ $stack['StackStatus'] == 'CREATE_COMPLETE'  ? 'list-group-item-primary' : '' }}"
                           aria-current="true">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{ $stack['StackName'] }}</h5>
                                <small>{{ $stack['StackStatus'] }}</small>
                            </div>
                            <p class="mb-1">{{ $stack['TemplateDescription'] }}</p>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
