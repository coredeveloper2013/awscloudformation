@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <div class="list-group">
                    <a href="{{ route('stack.index') }}" class="list-group-item list-group-item-action active"
                       aria-current="true">
                        Wordpress Site List
                    </a>
                    <a href="{{ route('stack.create') }}" class="list-group-item list-group-item-action">
                        Create Wordpress Site
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                Click right side
            </div>
        </div>
    </div>
@endsection
